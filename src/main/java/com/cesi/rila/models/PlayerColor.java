package com.cesi.rila.models;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum PlayerColor {
    RED,
    BLUE,
    YELLOW,
    GREEN,
    PURPLE,
    WHITE,
    GREY,
    PINK,
    BROWN,
    ORANGE;

    private static final List<PlayerColor> VALUES = Arrays.asList(values());
    private static final int SIZE = VALUES.size();
    private static final Random RANDOM = new Random();

    public static PlayerColor randomColor() {
        return VALUES.get(RANDOM.nextInt(SIZE));
    }
}
