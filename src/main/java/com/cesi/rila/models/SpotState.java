package com.cesi.rila.models;

public enum SpotState {
    OFF("OFF"),
    WILL_LIGHT("WILL_LIGHT"),
    LIT("LIT");

    private String state;

    SpotState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }
}
