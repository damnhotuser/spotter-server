package com.cesi.rila.models;

import java.time.Instant;

public class Spot extends PositionedElement {
    private SpotState state = SpotState.OFF;

    private long lastSpotlightTimestamp = Instant.now().toEpochMilli();
    private long timeBetweenSpotlights = 6000;
    private long spotlightIndicatorTimeBefore = 2000;
    private long spotlightDuration = 3000;

    public Spot() {
        super();

        this.diameter = Cave.HEIGHT / 5;
        this.x = Cave.WIDTH / 2 - this.diameter / 2;
        this.y = Cave.HEIGHT / 2 - this.diameter / 2;
    }

    public SpotState getState() {
        return state;
    }

    public void setState(SpotState state) {
        this.state = state;
    }

    public long getLastSpotlightTimestamp() {
        return lastSpotlightTimestamp;
    }

    public void setLastSpotlightTimestamp(long lastSpotlightTimestamp) {
        this.lastSpotlightTimestamp = lastSpotlightTimestamp;
    }

    public long getTimeBetweenSpotlights() {
        return timeBetweenSpotlights;
    }

    public void setTimeBetweenSpotlights(long timeBetweenSpotlights) {
        this.timeBetweenSpotlights = timeBetweenSpotlights;
    }

    public long getSpotlightIndicatorTimeBefore() {
        return spotlightIndicatorTimeBefore;
    }

    public void setSpotlightIndicatorTimeBefore(long spotlightIndicatorTimeBefore) {
        this.spotlightIndicatorTimeBefore = spotlightIndicatorTimeBefore;
    }

    public long getSpotlightDuration() {
        return spotlightDuration;
    }

    public void setSpotlightDuration(long spotlightDuration) {
        this.spotlightDuration = spotlightDuration;
    }
}
