package com.cesi.rila.models;

import java.net.SocketAddress;
import java.util.Objects;

public class Player extends PositionedElement implements Cloneable {
    public static final int DIAMETER = 24;
    public static final int VELOCITY = 8;

    private SocketAddress socketAddress;
    private String pseudonym;
    private PlayerColor color;
    private boolean isDead = false;

    public Player(SocketAddress socketAddress) {
        super();

        this.socketAddress = socketAddress;
        this.diameter = DIAMETER;
        this.x = (Cave.WIDTH / 2) - (this.diameter / 2);
        this.y = (Cave.HEIGHT / 2) - (this.diameter / 2);
    }

    public SocketAddress getSocketAddress() {
        return socketAddress;
    }

    public String getPseudonym() {
        return pseudonym;
    }

    public void setPseudonym(String pseudonym) {
        this.pseudonym = pseudonym;
    }

    public PlayerColor getColor() {
        return color;
    }

    public void setColor(PlayerColor color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean getIsDead() {
        return isDead;
    }

    public void kill() {
        this.isDead = true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return socketAddress.equals(player.socketAddress) &&
                pseudonym.equals(player.pseudonym) &&
                color == player.color;
    }

    @Override
    public Player clone() {
        try {
            return (Player) super.clone();
        } catch (CloneNotSupportedException e) {
            Player p = new Player(null);
            p.setX(this.getX());
            p.setY(this.getY());
            p.setColor(this.getColor());
            p.setPseudonym(this.getPseudonym());
            return p;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(socketAddress, pseudonym, color);
    }

    public boolean collidesWith(Player other) {
        return false;
    }
}
