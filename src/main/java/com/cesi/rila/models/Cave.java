package com.cesi.rila.models;

public class Cave {
    public static final int WIDTH = 724;
    public static final int HEIGHT = 698;

    public boolean hasElementOutside(PositionedElement element) {
        return element.x <= 0 || (element.x + element.diameter) >= WIDTH
                || element.y <= 0 || (element.y + element.diameter) >= HEIGHT;
    }
}
