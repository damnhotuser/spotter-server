package com.cesi.rila.models;

public abstract class PositionedElement {
    protected int diameter;
    protected int x;
    protected int y;

    public PositionedElement() {

    }

    public int getDiameter() {
        return diameter;
    }

    public int radius() { return diameter / 2; }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }


    public boolean hits(PositionedElement other) {
        // https://stackoverflow.com/questions/1736734/circle-circle-collision
        // (x2-x1)^2 + (y1-y2)^2 <= (r1+r2)^2
        return Math.pow(((other.getX() - other.diameter / 2) - (this.getX() - this.diameter / 2)), 2)
                + Math.pow(((this.getY() - this.diameter / 2) - (other.getY() - other.diameter / 2)), 2)
                <= Math.pow((this.diameter + other.diameter), 2);
    }
}
