package com.cesi.rila;

import com.cesi.rila.messages.ClientMessageCode;

import java.io.IOException;
import java.net.*;

public class SpottedClient implements Runnable {
    private DatagramSocket socket;
    private InetAddress inetAddress;

    public SpottedClient() throws SocketException, UnknownHostException {
        this.socket = new DatagramSocket();
        this.inetAddress = InetAddress.getByName("localhost");
    }

    @Override
    public void run() {
        /*while (true) {
            MoveRequest moveRequest = new MoveRequest(true, true, false, false);
            byte[] data = new byte[0];

            try {
                data = moveRequest.toJson().getBytes();
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            DatagramPacket packet = new DatagramPacket(data, data.length, this.inetAddress, SpottedServer.SERVER_PORT);

            try {
                socket.send(packet);
                Thread.sleep(3000);
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }*/

        try {
            byte[] data = ("{ \"message\": \"" + ClientMessageCode.JOIN.getMessage() + "\" }").getBytes();
            DatagramPacket packet = new DatagramPacket(data, data.length, this.inetAddress, SpottedServer.SERVER_PORT);

            socket.send(packet);
            DatagramPacket dataReceived = new DatagramPacket(new byte[1024], 1024);
            socket.receive(dataReceived);
            System.out.println("Data recieved : " + new String(dataReceived.getData()));

            Thread.sleep(1000);

            data = ("{ \"message\": \"" + ClientMessageCode.START_GAME.getMessage() + "\" }").getBytes();
            packet = new DatagramPacket(data, data.length, this.inetAddress, SpottedServer.SERVER_PORT);

            socket.send(packet);
            dataReceived = new DatagramPacket(new byte[1024], 1024);
            socket.receive(dataReceived);
            System.out.println("Data recieved : " + new String(dataReceived.getData()));

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
