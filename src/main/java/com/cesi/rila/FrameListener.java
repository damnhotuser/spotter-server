package com.cesi.rila;

import com.cesi.rila.GameModel;
import com.cesi.rila.Responder;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class FrameListener implements Runnable {
    private DatagramSocket socket;
    private GameModel model;

    final private static byte[] buffer = new byte[1024];

    public FrameListener(DatagramSocket socket, GameModel model) {
        this.socket = socket;
        this.model = model;
    }

    @Override
    public void run() {
        System.out.println("Listening for incoming frames...");

        while (true) {
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

            try {
                socket.receive(packet);
            } catch (IOException e) {
                e.printStackTrace();
            }

            new Thread(new Responder(socket, packet, model)).start();
        }
    }
}
