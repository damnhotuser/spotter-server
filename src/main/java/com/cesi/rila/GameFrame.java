package com.cesi.rila;

import com.cesi.rila.messages.ServerMessageCode;
import com.cesi.rila.models.Player;
import com.cesi.rila.models.Spot;

import java.util.ArrayList;
import java.util.List;

public class GameFrame {
    public static int frameNumber = 0;
    private String message = ServerMessageCode.GAME_FRAME.getMessage();

    private int number;
    private List<Player> players;
    private Spot spot;

    public GameFrame() {
        this.number = frameNumber + 1;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public Spot getSpot() {
        return spot;
    }

    public void setSpot(Spot spot) {
        this.spot = spot;
    }
}
