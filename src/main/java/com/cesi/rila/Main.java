package com.cesi.rila;

import java.net.SocketException;
import java.net.UnknownHostException;

public class Main {

    public static void main(String[] args) throws SocketException, UnknownHostException, InterruptedException {
        SpottedServer server = new SpottedServer();
        server.run();
    }
}
