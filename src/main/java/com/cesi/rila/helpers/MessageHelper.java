package com.cesi.rila.helpers;

import com.cesi.rila.GameFrame;
import com.cesi.rila.GameModel;
import com.cesi.rila.messages.ServerMessageCode;
import com.cesi.rila.models.Player;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MessageHelper {
    public static ObjectMapper mapper = new ObjectMapper();

    public static byte[] buildOkJoinMessage(InetAddress hostAddress, List<Player> players) throws JsonProcessingException {
        ObjectNode root = mapper.createObjectNode();
        root.put("message", ServerMessageCode.OK_JOIN.getMessage());
        root.put("host_addr", hostAddress.toString());

        ObjectNode playersNode = mapper.createObjectNode();
        for (Player p : players) {
            ObjectNode pNode = mapper.createObjectNode();

            pNode.put("pseudonym", p.getPseudonym());
            pNode.put("color", p.getColor().toString());

            playersNode.set(p.getPseudonym(), pNode);
        }
        root.set("players", playersNode);

        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root).getBytes();
    }

    public static byte[] buildPlayerJoinedMessage(Player player) throws JsonProcessingException {
        ObjectNode root = mapper.createObjectNode();
        root.put("message", ServerMessageCode.PLAYER_JOINED.getMessage());

        ObjectNode playerNode = mapper.createObjectNode();
        playerNode.put("pseudonym", player.getPseudonym());
        playerNode.put("color", player.getColor().toString());

        root.set("player", playerNode);

        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root).getBytes();
    }

    public static byte[] buildGameFrameMessage(GameModel model) throws JsonProcessingException {
        GameFrame frame = new GameFrame();
        frame.setPlayers(model.getPlayers());
        // model.getPlayers().stream().filter(Player::isDead).collect(Collectors.toList())
        frame.setSpot(model.getSpot());

        return mapper.writeValueAsString(frame).getBytes();
    }


    public static byte[] buildLobbyFullMessage() throws JsonProcessingException {
        ObjectNode root = mapper.createObjectNode();
        root.put("message", ServerMessageCode.LOBBY_FULL.getMessage());
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root).getBytes();
    }

    public static byte[] buildNotEnoughPlayersMessage() throws JsonProcessingException {
        ObjectNode root = mapper.createObjectNode();
        root.put("message", ServerMessageCode.NOT_ENOUGH_PLAYERS.getMessage());
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root).getBytes();
    }

    public static byte[] buildNotLobbyOwnerMessage() throws JsonProcessingException {
        ObjectNode root = mapper.createObjectNode();
        root.put("message", ServerMessageCode.NOT_LOBBY_OWNER.getMessage());
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root).getBytes();
    }
}
