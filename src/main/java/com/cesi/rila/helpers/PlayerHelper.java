package com.cesi.rila.helpers;

import com.cesi.rila.models.Player;
import com.cesi.rila.models.PlayerColor;

import java.net.InetAddress;
import java.net.SocketAddress;
import java.util.*;

public class PlayerHelper {
    private static Random random = new Random();

    private static List<String> DEFAULT_PSEUDONYMS = Arrays.asList(
            "CAPTAIN MAN",
            "IRON AMERICA",
            "HUKL",
            "SPIDERBOI",
            "MARTHOR",
            "WHITE WIDOW",
            "BLACK ANTHER",
            "THANUS",
            "DR HOUSE",
            "HAWK EARS"
    );

    public static String getValidPseudonym(ArrayList<Player> players, String requestedPseudonym) {
        if (requestedPseudonym == null)
            return getValidPseudonym(players, DEFAULT_PSEUDONYMS.get(random.nextInt(10)));

        for (Player c : players) {
            if (c.getPseudonym().equals(requestedPseudonym)) {
                return getValidPseudonym(players, DEFAULT_PSEUDONYMS.get(random.nextInt(10)));
            }
        }

        return requestedPseudonym;
    }

    public static PlayerColor getColor(ArrayList<Player> players) {
        PlayerColor color = PlayerColor.randomColor();
        for (Player c : players) {
            if (c.getColor().equals(color)) {
                return getColor(players);
            }
        }

        return color;
    }

    public static Player getPlayerFromInetAddr(List<Player> players, SocketAddress address) {
        for (Player p: players) {
            if (p.getSocketAddress().equals(address))
                return p;
        }

        return null;
    }
}
