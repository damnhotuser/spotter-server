package com.cesi.rila.messages;

public enum ClientMessageCode {
    JOIN ("100: JOIN"),
    START_GAME ("101: START GAME"),
    MOVE_REQUEST ("102: MOVE REQUEST");

    private String message;

    ClientMessageCode(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
