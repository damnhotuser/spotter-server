package com.cesi.rila.messages;

public enum ServerMessageCode {
    OK_JOIN ("300: OK JOIN"),
    PLAYER_JOINED ("301: PLAYER JOINED"),
    OK_START_GAME ("302: OK START GAME"),
    GAME_FRAME ("303: GAME FRAME"),

    LOBBY_FULL ("400: LOBBY FULL"),
    NOT_ENOUGH_PLAYERS ("401: NOT ENOUGH PLAYERS"),
    NOT_LOBBY_OWNER ("402: NOT LOBBY OWNER");

    private String messsage;

    ServerMessageCode(String messsage) {
        this.messsage = messsage;
    }

    public String getMessage() {
        return messsage;
    }
}
