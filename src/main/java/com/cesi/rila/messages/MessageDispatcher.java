package com.cesi.rila.messages;

import com.cesi.rila.models.Player;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketAddress;
import java.util.List;

public class MessageDispatcher {
    public static void dispatchToAllPlayers (DatagramSocket socket, List<Player> players, byte[] message) throws IOException {
        for (Player p: players) {
            dispatchToInetAddr(socket, p.getSocketAddress(), message);
        }
    }

    public static void dispatchToAllPlayersBut (DatagramSocket socket, List<Player> players, Player but, byte[] message) throws IOException {
        for (Player p: players) {
            if (!p.equals(but))
                dispatchToInetAddr(socket, p.getSocketAddress(), message);
        }
    }

    public static void dispatchToPlayer (DatagramSocket socket, Player player, byte[] message) {

    }

    public static void dispatchToInetAddr (DatagramSocket socket, SocketAddress address, byte[] message) throws IOException {
        DatagramPacket response = new DatagramPacket(message, message.length, address);
        socket.send(response);
    }
}
