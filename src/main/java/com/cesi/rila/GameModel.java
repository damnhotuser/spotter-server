package com.cesi.rila;

import com.cesi.rila.helpers.PlayerHelper;
import com.cesi.rila.models.*;

import java.net.SocketAddress;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class GameModel {
    public static int MIN_PLAYERS = 2;
    public static int MAX_PLAYERS = 10;

    private static final Random RANDOM = new Random();

    private GameState gameState = GameState.LOBBY;
    private Player owner;

    private Cave cave = new Cave();
    private ArrayList<Player> players = new ArrayList<>();
    private Spot spot = new Spot();

    private boolean acceptMovements = true;

    public boolean doesPlayerAlreadyExists(SocketAddress socketAddress) {
        for (Player p : this.getPlayers()) {
            if (p.getSocketAddress().equals(socketAddress)) {
                return true;
            }
        }
        return false;
    }

    public Player addPlayer(SocketAddress socketAddress, String pseudonym) {
        Player player = new Player(socketAddress);
        player.setPseudonym(PlayerHelper.getValidPseudonym(players, pseudonym));
        player.setColor(PlayerHelper.getColor(players));

        this.players.add(player);

        if (owner == null)
            this.owner = player;

        System.out.println("Adding new player to the lobby with IP " + socketAddress);
        return player;
    }

    public void eliminatePlayer(Player player) {
        player.kill();
    }

    public void initGame() {
        final int shift = (Cave.WIDTH - (this.players.size() * Player.DIAMETER)) / this.players.size();
        final int ypos = (Cave.HEIGHT / 2) - (Player.DIAMETER / 2);
        int actual = shift;

        for (Player p : this.players) {
            p.setX(actual);
            p.setY(ypos);

            actual += shift + Player.DIAMETER;
        }
    }

    public void clearLobby() {
        this.gameState = GameState.LOBBY;
        this.players.clear();
        this.spot = new Spot();
    }

    public void update() {
        long now = Instant.now().toEpochMilli();

        if (spot.getState() == SpotState.LIT) {
            acceptMovements = false;

            this.getPlayers().stream()
                    .filter(player -> !player.getIsDead())
                    .filter(this.spot::hits)
                    .forEach(this::eliminatePlayer);

            if (now - spot.getLastSpotlightTimestamp() >= spot.getSpotlightDuration()) {
                spot.setState(SpotState.OFF);
            }
        } else {
            acceptMovements = true;
            if (now - spot.getLastSpotlightTimestamp() >= spot.getTimeBetweenSpotlights()) {
                spot.setState(SpotState.LIT);
                spot.setLastSpotlightTimestamp(now);

            } else if (now - spot.getLastSpotlightTimestamp() >=
                    (spot.getTimeBetweenSpotlights() - spot.getSpotlightIndicatorTimeBefore())
                    && spot.getState() != SpotState.WILL_LIGHT) {
                spot.setX(RANDOM.nextInt(Cave.WIDTH) - spot.radius());
                spot.setY(RANDOM.nextInt(Cave.HEIGHT) - spot.radius());
                spot.setState(SpotState.WILL_LIGHT);
            }
        }
    }

    // === MOVEMENTS === //

    private void assignMoveRequest(MoveRequest moveRequest, Player player) {
        if (moveRequest.isTop())
            player.setY(player.getY() + Player.VELOCITY);
        if (moveRequest.isRight())
            player.setX(player.getX() + Player.VELOCITY);
        if (moveRequest.isBottom())
            player.setY(player.getY() - Player.VELOCITY);
        if (moveRequest.isLeft())
            player.setX(player.getX() - Player.VELOCITY);
    }

    public boolean canApplyMoveRequest(Player player, MoveRequest moveRequest) {
        if(!acceptMovements)
            return false;

        if (player == null || player.getIsDead())
            return false;

        // on crée un nouveau player et on lui applique la move request avant de checkerr
        // si celle ci est valide
        Player tempPlayer = player.clone();
        this.assignMoveRequest(moveRequest, tempPlayer);

        if (this.cave.hasElementOutside(tempPlayer))
            return false;

        return this.getPlayers().stream()
                .filter(Player::getIsDead)
                .noneMatch(p -> tempPlayer.hits(p) && !tempPlayer.equals(p));
    }

    public void applyMoveRequest(Player player, MoveRequest moveRequest) {
        assignMoveRequest(moveRequest, player);
    }

    // === GETTERS AND SETTERS === //

    public List<Player> getAlivePlayer() {
        return this.players.stream().filter(Player::getIsDead).collect(Collectors.toList());
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState game) {
        this.gameState = game;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public Spot getSpot() {
        return spot;
    }

    public Player getOwner() {
        return owner;
    }

    public void setOwner(Player owner) {
        this.owner = owner;
    }

    public boolean isAcceptMovements() {
        return acceptMovements;
    }

    public void setAcceptMovements(boolean acceptMovements) {
        this.acceptMovements = acceptMovements;
    }
}
