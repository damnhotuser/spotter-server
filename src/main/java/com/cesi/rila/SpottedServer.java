package com.cesi.rila;

import com.cesi.rila.helpers.MessageHelper;
import com.cesi.rila.messages.MessageDispatcher;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.SocketException;

public class SpottedServer {
    public static int SERVER_PORT = 9876;

    private DatagramSocket socket;
    private GameModel model;

    public SpottedServer() throws SocketException {
        System.out.println("Creating the server on port " + SERVER_PORT + "...");

        socket = new DatagramSocket(SERVER_PORT);
        model = new GameModel();
    }

    public void run() {
        new Thread(new FrameListener(this.socket, this.model)).start();

        while (true) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (this.model.getGameState() == GameState.GAME /*&& this.model.getPlayers().size() > 1*/) {
                model.update();

                try {
                    byte[] data = MessageHelper.buildGameFrameMessage(this.model);
                    //System.out.println(new String(data, "UTF-8"));
                    MessageDispatcher.dispatchToAllPlayers(this.socket, this.model.getPlayers(), data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
