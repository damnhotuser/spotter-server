package com.cesi.rila;

import com.cesi.rila.helpers.MessageHelper;
import com.cesi.rila.messages.ClientMessageCode;
import com.cesi.rila.messages.MessageDispatcher;
import com.cesi.rila.models.Player;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;


public class Responder implements Runnable {
    private DatagramSocket socket;
    private DatagramPacket packet;
    private GameModel model;
    private ObjectMapper mapper;

    public Responder(DatagramSocket socket, DatagramPacket packet, GameModel model) {
        this.socket = socket;
        this.packet = packet;
        this.model = model;
        this.mapper = new ObjectMapper();
    }

    public void run() {
        String received = new String(packet.getData(), StandardCharsets.UTF_8);
        //System.out.println(received);

        try {
            switch (this.model.getGameState()) {
                case LOBBY:
                    manageLobbyFrame(received);
                    break;
                case GAME:
                    manageGameFrame(received);
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // TODO MOVE THIS PART TO A CONTROLLER

    private void manageLobbyFrame(String received) throws IOException {
        JsonNode jsonNode = mapper.readTree(received);
        String clientMessage = jsonNode.get("message").asText();
        byte[] data;

        if (clientMessage.equals(ClientMessageCode.JOIN.getMessage())) {
            if (this.model.doesPlayerAlreadyExists(this.packet.getSocketAddress()))
                return;

            if (this.model.getPlayers().size() < GameModel.MAX_PLAYERS) {
                Player player = this.model.addPlayer(packet.getSocketAddress(),
                        (jsonNode.has("pseudonym") ? jsonNode.get("pseudonym").asText() : null));

                MessageDispatcher.dispatchToAllPlayersBut(this.socket, this.model.getPlayers(),
                        player, MessageHelper.buildPlayerJoinedMessage(player));

                data = MessageHelper.buildOkJoinMessage(InetAddress.getByName("localhost"), model.getPlayers());
            } else {
                data = MessageHelper.buildLobbyFullMessage();
            }

            MessageDispatcher.dispatchToInetAddr(this.socket, packet.getSocketAddress(), data);

        } else if (clientMessage.equals(ClientMessageCode.START_GAME.getMessage())) {
            /*if (this.model.getPlayers().size() < GameModel.MIN_PLAYERS) {
                data = MessageHelper.buildNotEnoughPlayersMessage();
                MessageDispatcher.dispatchToInetAddr(this.socket, packet.getSocketAddress(), data);
            } else*/
            if (!this.model.getOwner().getSocketAddress().equals(packet.getSocketAddress())) {
                data = MessageHelper.buildNotLobbyOwnerMessage();
                MessageDispatcher.dispatchToInetAddr(this.socket, packet.getSocketAddress(), data);
            } else {
                this.model.initGame();
                this.model.setGameState(GameState.GAME);
            }
        }
    }

    private void manageGameFrame(String received) throws IOException {
        JsonNode jsonNode = mapper.readTree(received);
        String clientMessage = jsonNode.get("message").asText();
        byte[] data;

        if (!this.model.doesPlayerAlreadyExists(this.packet.getSocketAddress()))
            return;

        if (clientMessage.equals(ClientMessageCode.MOVE_REQUEST.getMessage())) {
            MoveRequest moveRequest = new MoveRequest(jsonNode.get("top").asBoolean(),
                    jsonNode.get("right").asBoolean(),
                    jsonNode.get("bottom").asBoolean(),
                    jsonNode.get("left").asBoolean());

            Player player = null;
            for (Player p : this.model.getPlayers()) {
                if (p.getSocketAddress().equals(this.packet.getSocketAddress())) {
                    player = p;
                    break;
                }
            }
            if (this.model.canApplyMoveRequest(player, moveRequest))
                this.model.applyMoveRequest(player, moveRequest);
        }
    }
}
